// libs
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const _ = require('lodash')

const matches = require('./static/json/matches.json')

const mongoConnection = require('./mongoConnection')

const app = express()
const PORT = process.env.PORT || 5000

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.post('/play_bingo', (req, res) => {

    let { card = '', number_list = '' } = req.body

    if (card.length === 0 || number_list === 0) return res.json({ success: false, message: 'Send field is error.' })

    card = card.split(',')

    number_list = number_list.split(',')

    if (card.length !== 25) return res.json({ success: false, message: 'Number in the card must be equal to 25.' })

    unipCard = _.uniq(card)

    unipNumberList = _.uniq(number_list)

    if (number_list.length !== unipNumberList.length || card.length !== unipCard.length) return res.json({ success: false, message: 'Sent value duplicate.' })

    card = _.map(card, item => +item)

    card = _.chunk(card, 5)

    number_list = _.map(number_list, item => +item)

    const careLandscape = (arrCard, arrNumberList) => {

        let arrConditionLandscape = []

        for (let i = 0; i < arrCard.length; i++) {

            const landscapeDiff = _.difference(arrCard[i], arrNumberList)

            arrConditionLandscape.push(landscapeDiff.length === 0)

        }

        return arrConditionLandscape.includes(true)

    }

    const careCertical = (arrCard, arrNumberList) => {

        let arrCardCareCertical = []

        for (let i = 0; i < arrCard.length; i++) for (let item of arrCard) arrCardCareCertical.push(item[i])

        arrCardCareCertical = _.chunk(arrCardCareCertical, 5)

        return careLandscape(arrCardCareCertical, arrNumberList)

    }

    const careDiagonalTopฺBottom = (arrCard, arrNumberList) => {

        let arrConditioDiagonalTopฺBottom = []

        for (let i = 0; i < arrCard.length; i++) arrConditioDiagonalTopฺBottom.push(arrCard[i][i])
        return _.difference(arrConditioDiagonalTopฺBottom, arrNumberList).length === 0

    }

    const careDiagonalBottomTop = (arrCard, arrNumberList) => {

        let arrConditiotDiagonalBottomTop = []

        let y = 0

        for (let x = arrCard.length - 1; x >= 0; x--) {
            arrConditiotDiagonalBottomTop.push(arrCard[x][y])
            y++
        }

        return _.difference(arrConditiotDiagonalBottomTop, arrNumberList).length === 0
    }


    if (careLandscape(card, number_list) ||
        careCertical(card, number_list) ||
        careDiagonalTopฺBottom(card, number_list) ||
        careDiagonalBottomTop(card, number_list)
    ) return res.json({ success: true, isBingo: true })

    return res.json({ success: true, isBingo: false })
})

app.get('/matches/:id', async (req, res) => {

    const { id: menber_id } = req.params

    let arrMatches = []

    for (let item of matches.data) {

        const dataUserPredict = await modelGetUserPredict(item.match_id, menber_id)

        arrMatches.push({
            ...item,
            is_show_edit: dataUserPredict ? true : false
        })

    }

    arrMatches = _.orderBy(arrMatches, ['match_start'], ['asc'])

    return res.json({ success: true, data: arrMatches })

})

const modelGetUserPredict = async (match_id, menber_id) => {

    return await mongoConnection(async (db) => {

        const where = {
            menber_id: +menber_id,
            match_id: match_id
        }

        const userPredict = await db.collection('user_predict').findOne(where)

        return userPredict

    })
}

// app.get('/get_user_predict', async (req, res) => {

//     const { menber_id, match_id } = req.query



//     }

//     const dataUserPredict = await modelGetUserPredict()

//     const json = dataUserPredict ? { success: true, data: dataUserPredict } : { success: false }

//     return res.json(json)

// })

app.post('/upsert_user_predict', async (req, res) => {

    const { menber_id, match_id, predict, credit } = req.body

    const modelupsertUserPredict = async () => {

        return await mongoConnection(async (db) => {

            const where = {
                menber_id: +menber_id,
                match_id: match_id
            }

            const userPredict = await db.collection('user_predict').findOne(where)

            if (userPredict) {

                const whereUpdate = {
                    menber_id: +menber_id,
                    match_id: match_id
                }

                const dataUpdate = {
                    $set: {
                        predict: predict,
                        credit: +credit
                    }
                }


                const userPredictInsert = await db.collection('user_predict').updateOne(whereUpdate, dataUpdate)

                if (userPredictInsert) return true

            }


            const dataInsert = {
                menber_id: +menber_id,
                match_id: match_id,
                predict: predict,
                credit: +credit
            }

            const userPredictUpdate = await db.collection('user_predict').insertOne(dataInsert)

            if (userPredictUpdate) return true

        })
    }

    const conditionStatus = await modelupsertUserPredict()

    if (conditionStatus) return res.json({ success: true })

})

app.listen(PORT, () => {
    console.log(`Server listening on pord : ${PORT}`)
})

module.exports = app