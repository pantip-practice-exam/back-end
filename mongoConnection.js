// libs
const { MongoClient } = require("mongodb")

const mongoConnection = async (callback, errorCallback = null) => {

    let conn = null

    try {
        // authenticate DB
        conn = await MongoClient.connect(`mongodb+srv://${`not17033`}:${`not17033`}@${`cluster0-vjwn4.mongodb.net`}`, { useNewUrlParser: true, useUnifiedTopology: true })

        return await callback(conn.db(`ball_game`))

    } catch (e) {
        // eslint-disable-next-line no-console
        console.log('Database connection error: ', e)
        if (errorCallback !== null) {
            return errorCallback(e)
        }
    } finally {
        if (conn !== null) {
            conn.close()
            //console.log('Database connection closed')
        }
    }

    return null
}

module.exports = mongoConnection
